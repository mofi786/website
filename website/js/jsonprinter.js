angular.module('JSONPrinter', [])
.controller('JSONCtrl', ['$scope', function($scope){
  var obj = $scope.obj = {
    name: 'Eugene Yang',
    age: 22,
    highschool: 'Mission San Jose',
    family: {
      mom: 'Eunjung',
      dad: 'Tom',
      brother: 'Elliot'
    }
  };
}])
.directive('afAdm', ['$compile', function($compile){
  return {
    restrict: 'E',
    scope: { value: '='},
    link: function(scope, element, attrs){
      const val = scope.value;
      if(angular.isString(val) || angular.isNumber(val)){
        element.append('<span>'+ val + '</span>');
      }
      else if(angular.isArray(val)){
        $compile('<div class="array"><af-adm ng-repeat="el in value" value="el"></af-adm></div>')(scope, function(cloned, scope){
          element.append(cloned);
        });
      }
      else if(angular.isObject(val)){
        $compile('<div class="record"><div class="datum" ng-repeat="(k,v) in value">{{k}}: <af-adm value="v"></af-adm></div></div>')
        (scope, function(cloned, scope){
          element.append(cloned);
        });
      }
    }
  };
}]);
