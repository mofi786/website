angular.module('Mo', ['ngRoute'])
.controller('MainController', function($scope, $window, $location){
  
// bxSlider stuff
//$(document).ready(function(){
  $('.bxslider').bxSlider({
	//adaptiveHeight: true,
	speed: 500,
	auto: true,
	pause: 5000,
	mode: 'fade',
	captions: true,
	slideWidth: 500
	//responsive: false
  });
  
})
.controller('BaseController', function($scope, $location){
  // sets variable to see if the current page is the home page or not
  $scope.$watch(function(){ return $location.path(); }, function(newVal, oldVal){
    $scope.isHome = (newVal == '/home');
    
    const container = $('#container');
    
    if($scope.isHome){
      container.removeClass('other').addClass('home');
    }
    else{
      container.removeClass('home').addClass('other');
    }
  });
})
.controller('LoadVideoController', function($scope, $location){
  // sets variable to see if the current page is the home page or not
  
  $('#wofvideo').load();
})
.config(function($routeProvider){
  $routeProvider
  .when('/home', {controller: 'MainController', templateUrl: '/partials/home.html'})
  .when('/projects', {controller: 'MainController', templateUrl: '/partials/projects.html'})
  .when('/work_intel', {controller: 'MainController', templateUrl: '/partials/work_intel.html'})
  .when('/work_broadcom', {controller: 'MainController', templateUrl: '/partials/work_broadcom.html'})
  .when('/work_hp', {controller: 'MainController', templateUrl: '/partials/work_hp.html'})
  .when('/work_calplug', {controller: 'MainController', templateUrl: '/partials/work_calplug.html'})
  .when('/work_bec', {controller: 'MainController', templateUrl: '/partials/work_bec.html'})

  .when('/about', {controller: 'LoadVideoController', templateUrl: '/partials/about.html'})
  .when('/articles', {controller: 'MainController', templateUrl: '/partials/articles.html'})
  .when('/projects_ezlock', {controller: 'MainController', templateUrl: '/partials/projects_ezlock.html'})
  .when('/projects_tc', {controller: 'MainController', templateUrl: '/partials/projects_tc.html'})
  .when('/projects_bo', {controller: 'MainController', templateUrl: '/partials/projects_bo.html'})
  //.when('/comments', {controller: 'MainController', templateUrl: '/partials/comments.html'})

  .otherwise({ redirectTo: '/home' });
})


// Music player stuff
$(function(){
  const player = $('#musicplayer')[0];
  const songRoot = '/media/';
  const songs = [
    'fortunedays.mp3',
    'dreamwithinadream.mp3'
  ];
  
  var next = 0;
  
  function playNext(){
    player.src = songRoot + songs[next];
    player.play();
  }
  
  player.addEventListener('ended', function(){
    next = (next+1) % songs.length;
    playNext();
  });
  
  //playNext();
  
  
});

